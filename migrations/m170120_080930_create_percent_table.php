<?php

use yii\db\Migration;

/**
 * Handles the creation of table `percent`.
 */
class m170120_080930_create_percent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('percent', [
            'id' => $this->primaryKey(),
            'percent' => $this->float()->notNull(),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);

        $this->insert('percent',['percent' => 0]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('percent');
    }
}
