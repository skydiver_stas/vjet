<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users`.
 */
class m170118_174009_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'aut_key' => $this->string(32),
            'access_token' => $this->string(32),
            'create_at' => $this->timestamp(),
            'update_at' => $this->timestamp(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('users');
    }
}
