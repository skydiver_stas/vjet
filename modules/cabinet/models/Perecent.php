<?php

namespace app\modules\cabinet\models;

use Yii;

/**
 * This is the model class for table "percent".
 *
 * @property integer $id
 * @property double $percent
 * @property string $create_at
 * @property string $update_at
 */
class Perecent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'percent';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['percent'], 'required'],
            [['percent'], 'number'],
            [['create_at', 'update_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'percent' => 'Percent',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
        ];
    }

    public static function updatePercent($percent_from_form)
    {
        $percent = self::findOne(['id' => 1]);
        $percent->percent = $percent_from_form;
        return $percent->update();
    }

    public static function getPercent()
    {
        $percent = self::findOne(['id' => 1])->toArray();
        return $percent['percent'];
    }
}
