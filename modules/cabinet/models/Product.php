<?php

namespace app\modules\cabinet\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $name
 * @property double $price
 * @property double $shipping_price
 * @property string $create_at
 * @property string $update_at
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $url_store;

    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['url_store'], 'required'],
            [['price'], 'number'],
            [['create_at', 'update_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'shipping_price' => 'Shipping Price',
            'create_at' => 'Create At',
            'update_at' => 'Update At',
            'url_store' => 'Введите адрес страници с товаром с таких сайтов как "aliexpress.com taobao.com ebay.com amazon.com"',
        ];
    }
}
