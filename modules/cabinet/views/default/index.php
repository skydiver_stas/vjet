<?php

use yii\helpers\Html;

?>
<div class="cabinet-default-index">
    <h1><?= $this->context->action->uniqueId ?></h1>
    <?= Html::a('Изменить процент', ['percent/index'], ['class'=>'btn btn-primary']) ?>
    <?= Html::a('Добавить товар', ['product/index'], ['class'=>'btn btn-primary']) ?>

</div>
