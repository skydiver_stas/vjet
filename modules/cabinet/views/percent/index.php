<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Изминение процента';
$this->params['breadcrumbs'][] = ['label' => 'Кабинет', 'url' => ['/cabinet']];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'percent')->textInput(['maxlength' => true, 'value' => $percent]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Изменить' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>