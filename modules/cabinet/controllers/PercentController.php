<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\modules\cabinet\models\Perecent;
use app\models\UploadForm;

class PercentController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $model = new Perecent();


        if(Yii::$app->request->post('Perecent')) {
            Perecent::updatePercent(Yii::$app->request->post('Perecent')['percent']);
        }
        $percent = Perecent::getPercent();
        return $this->render('index',[
            'model' => $model,
            'percent' => $percent,
        ]);
    }

}
