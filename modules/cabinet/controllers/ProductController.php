<?php

namespace app\modules\cabinet\controllers;

use Yii;
use app\modules\cabinet\Access;
use app\modules\cabinet\models\Product;
use app\modules\cabinet\models\SearchProduct;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use mylibraries\Parser;
use mylibraries\Mod;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Access
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SearchProduct();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        //var_dump($model);die;
        if ($model->load(Yii::$app->request->post())) {

            $data = [];
            $array_patterns = [
                'aliexpress' => '/aliexpress/',
                'taobao' => '/taobao/',
                'ebay' => '/ebay/',
                'amazon' => '/amazon/',
            ];

            $url_store = Yii::$app->request->post('Product')['url_store'];
            $url_store_array = explode('/', $url_store);

            foreach($array_patterns as $key => $value) {

                if(preg_match($value, $url_store_array[2])) {

                    switch($key) {

                        case 'aliexpress':
                            $data = Parser::Aliexpress($url_store);
                            break;

                        case 'taobao':
                            $data = Parser::Taobao($url_store);
                            break;

                        case 'ebay':
                            $data = Parser::Ebay($url_store);
                            break;

                        case 'amazon':
                            $data = Parser::Amazon($url_store);
                            break;
                    }

                } else {

                }

                if(!empty($data)) {

                    return $this->render('_create', [
                        'model' => $model,
                        'data' => $data,
                    ]);
                }

            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionAdd() {
        $model = new Product();

        //var_dump($model);die;
        if ($model->load(Yii::$app->request->post())) {

            $model->name = Yii::$app->request->post('Product')['name'];
            $model->price = Yii::$app->request->post('Product')['price'];

            if($model->save()) {
                return $this->redirect(['index']);
            } else {
                return $this->redirect(['create']);
            }

        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
