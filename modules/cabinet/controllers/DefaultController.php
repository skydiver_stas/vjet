<?php

namespace app\modules\cabinet\controllers;

use yii\web\Controller;
use app\modules\cabinet\Access;

/**
 * Default controller for the `cabinet` module
 */
class DefaultController extends Access
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
