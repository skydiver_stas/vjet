<?php


namespace app\modules\cabinet;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;

class Access extends Controller{


    public function beforeAction($action)
    {

        if (!parent::beforeAction($action))
        {
            return false;
        }

        if (!Yii::$app->user->isGuest)
        {
            return true;
        }
        else
        {
            Yii::$app->getResponse()->redirect(Url::toRoute(['/site/login']));
            //для перестраховки вернем false
            return false;
        }
    }
} 