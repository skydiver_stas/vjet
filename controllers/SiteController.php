<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Product;
use app\models\Percent;
use mylibraries\Parser;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $product_list = Product::getAllProducts();
        $percent = Percent::getPercent();
        $valyutay = Parser::getExchange();

        $currency = null;

        $currency_rub = null;
        $currency_eur = null;
        $currency_uah = null;

        $i=0;
        while(isset($valyutay->Valute[$i])) {

            if($valyutay->Valute[$i]->CharCode == 'USD') {
                $nominal_usd = (Float) $valyutay->Valute[$i]->Nominal;
                $value_usd = (Float) $valyutay->Valute[$i]->Value;
                $currency_rub = (Float) $value_usd / (Float) $nominal_usd;

            }

            if($valyutay->Valute[$i]->CharCode == 'EUR') {
                $nominal_eur = $valyutay->Valute[$i]->Nominal;
                $value_eur = $valyutay->Valute[$i]->Value;

            }

            if($valyutay->Valute[$i]->CharCode == 'UAH') {
                $nominal_uah = $valyutay->Valute[$i]->Nominal;
                $value_uah = $valyutay->Valute[$i]->Value;

            }

            $i++;
        }

        $currency_eur = ((Float) $nominal_eur / (Float) $value_eur) * $currency_rub;
        $currency_uah = ((Float) $nominal_uah / (Float) $value_uah) * $currency_rub;

        $currency_eur = number_format($currency_eur, 2, '.', '');
        $currency_uah = number_format($currency_uah, 2, '.', '');

        $currency = ['currency_rub' => (Float) $currency_rub, 'currency_eur' => (Float) $currency_eur, 'currency_uah' => (Float) $currency_uah];

        for($i = 0; $i < count($product_list); $i++) {
            $product_list[$i]['price'] += (Float) $product_list[$i]['price'] * (Float) $percent / 100 ;
        }

        return $this->render('index',[
            'product_list' => $product_list,
            'currency' => $currency,
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
