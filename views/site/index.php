<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';

?>
<div class="site-index">


        <h1>Congratulations!</h1>
        <p><div>
                <div>1$ = <?= $currency['currency_uah'] ?> UAH</div>
                <div>1$ = <?= $currency['currency_eur'] ?> EUR</div>
                <div>1$ = <?= $currency['currency_rub'] ?> RUR</div>
        </div></p>

        <?php foreach($product_list as $key): ?>

            <p><div>
                    <div><?= $key['name'] ?></div>
                    <div>$<?= $key['price'] ?></div>
            </div></p>

        <?php endforeach; ?>

</div>
