<?php
/**
 * Created by PhpStorm.
 * User: stas
 * Date: 19.01.17
 * Time: 11:40
 */

namespace mylibraries;

use Yii;
use GuzzleHttp\Client;
use yii\helpers\Url;

class Parser
{
    public static function getDocument($url_store)
    {
        $options = [
            'headers' => [
                'Accept-Language' => 'en-us',
            ]
        ];

        $client = new Client();
        $request = $client->request('GET', $url_store);
        $body = $request->getBody();
        return $document = \phpQuery::newDocumentHTML($body);
    }

    public static function getExchange()
    {
        $client = new Client();
        $request = $client->request('GET', 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date("d/m/Y"));// 19/01/2017
        $body = $request->getBody();
        $document_xml = \phpQuery::newDocument($body);
        return $xml = simplexml_load_string($document_xml);
    }

    public static function Amazon($url_store)
    {
        $clear_price = '';

        $document = self::getDocument($url_store);
        $target = $document->find('div.centerColAlign');

            $name = pq($target)->find('span#productTitle');
            $name = pq($name)->text();
            $clear_name = trim($name);

            $price = pq($target)->find('span#priceblock_ourprice');
            $price = pq($price)->text();

            for($i = 0; $i < strlen($price); $i++) {

                if($price{$i} != '$') {
                    $clear_price .= $price{$i};
                }
            }

            $shiping = pq($target)->find('span.a-size-base');
            $shiping = pq($shiping)->text();
            $clear_shipping = null;

            if(preg_match('/Free Shipping/', $shiping)) {
                $clear_shipping = 0;
            } else if(preg_match('/$/', $shiping)) {

                $array_shipping = explode(' ', $shiping);

                for($i = 0; $i < count($array_shipping); $i++){
                    if(preg_match('/[0-9]/', $array_shipping[$i])) {
                        $clear_shipping = trim($array_shipping[$i]);
                        $clear_shipping = preg_replace('/[$]/', '', $clear_shipping);
                    }
                }
            }

        $clear_price = (Float) $clear_price + (Float) $clear_shipping;
        $info = ['name' => $clear_name, 'price' => $clear_price];


        return $info;
    }

    public static function Aliexpress($url_store)
    {
        $info = [];
        $price_usd = '';

        $document = self::getDocument($url_store);
        $target = $document->find('div.detail-wrap');

            $name = pq($target)->find('h1.product-name');
            $name = pq($name)->text();

            $shipping = pq($target)->find('span.logistics-cost');
            $shipping = pq($shipping)->text();
            $shipping = 0;

            $price = pq($target)->find('span#j-sku-price');
            $price = pq($price)->text();
            $price = explode(' - ', $price);
            $price = array_shift($price);

            $currency =  pq($target)->find('span.p-symbol');
            $currency =  pq($currency)->attrs('content');


        $client = new Client();
        $request = $client->request('GET', 'http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date("d/m/Y"));// 19/01/2017
        $body = $request->getBody();
        $document_xml = \phpQuery::newDocument($body);
        $xml = simplexml_load_string($document_xml);



        if($currency[0] == 'RUB') {
            $i=0;
            while(isset($xml->Valute[$i])) {

                if($xml->Valute[$i]->CharCode == 'USD') {
                    $nominal = $xml->Valute[$i]->Nominal;
                    $value = $xml->Valute[$i]->Value;

                }
                $i++;
            }
            for($i = 0; $i < strlen($price); $i++) {

                $foo = (Int)$price{$i};
                $foo = (String)$foo;

                if($price{$i} == $foo || $price{$i} == ',') {
                    $price_usd .= $price{$i};
                }

            }
            $price_usd = (((Float) $price_usd  + (Float) $shipping)/ (Float) $value) * $nominal;
            $info = ['name' => $name, 'price' => $price_usd ];
        }

        return $info;
    }

    public static function Taobao($url_store)
    {
        $info = [];
        $price_cny = '';
        $price_rub = null;
        $price_usd = null;
        $shipping = 0;

        $document = self::getDocument($url_store);
        $target = $document->find('div.detail-bd-rt');

            $name = pq($target)->find('span.t-title');
            $name = pq($name)->text();

            $price = pq($target)->find('strong.tb-rmb-num');
            $price = pq($price)->text();


            for($i = 0; $i < strlen($price); $i++){
                $foo = (Int)$price{$i};
                $foo = (String)$foo;

                if($price{$i} == $foo || $price{$i} == '.') {
                    $price_cny .= $price{$i};
                }
            }
        $xml = self::getExchange();
        $i = 0;
        while(isset($xml->Valute[$i])) {

            if($xml->Valute[$i]->CharCode == 'CNY') {
                $nominal_cny = $xml->Valute[$i]->Nominal;
                $value_cny = $xml->Valute[$i]->Value;

            }

            if($xml->Valute[$i]->CharCode == 'USD') {
                $nominal_usd = $xml->Valute[$i]->Nominal;
                $value_usd = $xml->Valute[$i]->Value;

            }

            $i++;
        }

        $price_rub = ((Float) $price_cny * (Float) $value_cny) / (Float) $nominal_cny;

        $price_usd = (((Float) $price_rub  + (Float) $shipping)/ (Float) $value_usd) * $nominal_usd;
        $info = ['name' => $name, 'price' => $price_usd ];

        return $info;
    }

    public static function Ebay($url_store)
    {
        $document = self::getDocument($url_store);
        $target = $document->find('div#CenterPanelInternal');

        $name = pq($target)->find('h1.it-ttl');
        $name = pq($name)->text();

        $price = pq($target)->find('span.notranslate');
        $price = pq($price)->attrs('content');
        for($i = 0; $i < count($price); $i++) {
            if(preg_match('/([0-9]+)/' ,$price[$i])){
                $clear_price = $price[$i];
            }
        }
        $price = $clear_price;

        $shipping = pq($target)->find('span.notranslate > span');
        $shipping = pq($shipping)->text();
        $clear_shipping = '';

        for($i = 0; $i < strlen($shipping); $i++) {

            if($shipping{$i} != '$') {
                $clear_shipping .= $shipping{$i};
            }
        }

        $clear_price = (float) $clear_shipping + (Float) $price;
        $info = ['name' => $name, 'price' => $clear_price];

        return $info;
    }
}